#!/bin/bash

PROJECT_DIRECTORY=$(realpath "$(dirname "$(realpath "${BASH_SOURCE[0]}")")/..")
COVERAGE_FUZZING_SCHEMA="$PROJECT_DIRECTORY/dist/coverage-fuzzing-report-format.json"

source "$PROJECT_DIRECTORY/test/helper_functions.sh"
source "$PROJECT_DIRECTORY/test/common-tests.sh"

setup_suite() {
  regenerate_dist_schemas
}

test_coverage_fuzz_contains_common_definitions() {
  ensure_common_definitions "$COVERAGE_FUZZING_SCHEMA" '["coverage_fuzzing"]'
}

test_coverage_fuzzing_extensions() {
  local vulns=".properties.vulnerabilities.items"
  local vuln_props="$vulns.properties"

  verify_schema_contains_selector "$COVERAGE_FUZZING_SCHEMA" 'select(.properties.vulnerabilities.items.required[] | contains("location"))'

  verify_schema_contains_selector "$COVERAGE_FUZZING_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.crash_address"
  verify_schema_contains_selector "$COVERAGE_FUZZING_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.stacktrace_snippet"
  verify_schema_contains_selector "$COVERAGE_FUZZING_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.crash_state"
  verify_schema_contains_selector "$COVERAGE_FUZZING_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.crash_type"
}

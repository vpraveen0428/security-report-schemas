import b from './builders/index'
import {schemas} from './support/schemas'

describe('cluster image scanning schema', () => {

  it('should validate location', () => {
    const report = b.cluster_image_scanning.report({
      vulnerabilities: [b.cluster_image_scanning.vulnerability({
        location: {
          dependency: {
            package: {name: 'bzip2'},
            version: '1.0.6-8.1'
          },
          operating_system: 'debian:9',
          image: 'registry.com/product/webgoat-8.0@sha256:bc09fe2e0',
          kubernetes_resource: {
            namespace: 'production',
            kind: 'Deployment',
            container_name: 'debian',
            name: 'debian-deployment'
          }
        }
      })]
    })

    expect(schemas.cluster_image_scanning.validate(report).success).toBeTruthy()
  })

  it('location dependency is required', () => {
    const report = b.cluster_image_scanning.report({
      vulnerabilities: [b.cluster_image_scanning.vulnerability({
        location: {
          operating_system: 'debian:9',
          image: 'registry.com/product/webgoat-8.0@sha256:bc09fe2e0',
          kubernetes_resource: {
            namespace: 'production',
            kind: 'Deployment',
            container_name: 'debian',
            name: 'debian-deployment'
          }
        }
      })]
    })

    expect(schemas.cluster_image_scanning.validate(report).errors).toContain('location requires property "dependency"')
  })

  it('location image is required', () => {
    const report = b.cluster_image_scanning.report({
      vulnerabilities: [b.cluster_image_scanning.vulnerability({
        location: {
          dependency: {
            package: {name: 'bzip2'},
            version: '1.0.6-8.1'
          },
          operating_system: 'debian:9',
          kubernetes_resource: {
            namespace: 'production',
            kind: 'Deployment',
            container_name: 'debian',
            name: 'debian-deployment'
          }
        }
      })]
    })

    expect(schemas.cluster_image_scanning.validate(report).errors).toContain('location requires property "image"')
  })

  it('location kubernetes resource is required', () => {
    const report = b.cluster_image_scanning.report({
      vulnerabilities: [b.cluster_image_scanning.vulnerability({
        location: {
          dependency: {
            package: {name: 'bzip2'},
            version: '1.0.6-8.1'
          },
          operating_system: 'debian:9',
          image: 'registry.com/product/webgoat-8.0@sha256:bc09fe2e0',
        }
      })]
    })

    expect(schemas.cluster_image_scanning.validate(report).errors).toContain('location requires property "kubernetes_resource"')
  })
})

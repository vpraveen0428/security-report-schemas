#!/usr/bin/env bash

set -e

PROJECT_DIRECTORY=$(realpath "$(dirname "$(realpath "${BASH_SOURCE[0]}")")/..")

# shellcheck disable=SC1090 # (dont follow, will be checked separately)
source "$PROJECT_DIRECTORY/scripts/changelog-utils.sh"

log() {
  printf "\n\n######### %s #########\n" "$*" >>/dev/stdout
}

GITLAB_REPOSITORY="${GITLAB_REPOSITORY:-https://gitlab.com/gitlab-org/gitlab.git}"
AUTHENTICATED_GITLAB_REPOSITORY="${GITLAB_REPOSITORY/https:\/\//https://gitlab-ci-token:$PUSH_TO_GITLAB_TOKEN@}"
MOST_RECENT_VERSION="$(changelog_last_version)"
VERSION="${MOST_RECENT_VERSION#v}"
GITLAB_SCHEMAS_DIRECTORY="ee/lib/ee/gitlab/ci/parsers/security/validators/schemas/$VERSION"
BRANCH_NAME="upgrade-secure-schemas-to-version-$VERSION"
SCHEMAS_PROJECT_URL=${CI_PROJECT_URL:-"https://gitlab.com/gitlab-org/security-products/security-report-schemas"}
CI_API_V4_URL=${CI_API_V4_URL:-"https://gitlab.com/api/v4"}
CI_JOB_URL=${CI_JOB_URL:-""}

# Verify appropriate environment variables are set
: "${PUSH_TO_GITLAB_TOKEN:?"Aborting, environment variable PUSH_TO_GITLAB_TOKEN must contain a token that has write/api access to GitLab Rails."}"
: "${PUSH_TO_RAILS_COMMIT_EMAIL:?"Aborting, environment variable PUSH_TO_RAILS_COMMIT_EMAIL must a valid email."}"
: "${PUSH_TO_RAILS_COMMIT_USERNAME:?"Aborting, environment variable PUSH_TO_RAILS_COMMIT_USERNAME must a valid name."}"
: "${GITLAB_PROJECT_ID:?"Aborting, environment variable GITLAB_PROJECT_ID must be the project ID of the GitLab Rails project."}"
: "${USER_ID_TO_ASSIGN_MR_TO:?"Aborting, environment variable USER_ID_TO_ASSIGN_MR_TO must be ID of the user that the MR should be assigned to once created. You can find out your userID by logging into GitLab.com and navigating to $CI_API_V4_URL/user."}"

# publish_schemas_to_gitlab_rails clones gitlab, copies schemas to a known location and pushes the changes to a branch
publish_schemas_to_gitlab_rails() {
  log "Cloning GitLab repository"
  git clone --depth=1 --single-branch "$AUTHENTICATED_GITLAB_REPOSITORY" gitlab

  log "Committing and pushing changes to branch $BRANCH_NAME"
  cd gitlab
  git checkout -b "$BRANCH_NAME"
  mkdir "$GITLAB_SCHEMAS_DIRECTORY"
  cp "$PROJECT_DIRECTORY"/dist/*.json "$GITLAB_SCHEMAS_DIRECTORY"
  git add "$GITLAB_SCHEMAS_DIRECTORY"
  git config user.email "$PUSH_TO_RAILS_COMMIT_EMAIL"
  git config user.name "$PUSH_TO_RAILS_COMMIT_USERNAME"
  git commit --no-gpg-sign --message "Add secure schemas version $VERSION"
  git push origin "$BRANCH_NAME"
}

# create_mr_to_merge_schemas creates an MR in GitLab rails and assigns it to a user
create_mr_to_merge_schemas() {
  local new_mr_url="$CI_API_V4_URL/projects/$GITLAB_PROJECT_ID/merge_requests"
  local title="Add Secure schemas version $VERSION"
  local source_branch="$BRANCH_NAME"
  local target_branch="master"
  local assignee_id="$USER_ID_TO_ASSIGN_MR_TO"
  local remove_source_branch="true"
  local description=""
  description="$description ## What does this MR do?\n\n"
  description="$description Adds the latest version of the [Secure schemas]($SCHEMAS_PROJECT_URL) into GitLab Rails.\n"
  description="$description \n"
  description="$description ## Background\n\n"
  description="$description Each GitLab Secure analyzer produces a JSON report that contains vulnerability findings to display on the Vulnerability Dashboard.\n"
  description="$description \n"
  description="$description GitLab [Secure schemas]($SCHEMAS_PROJECT_URL) are used to ensure that reports produced by analyzers are able to be parsed successfully by GitLab Rails."
  description="$description Each JSON report indicates which version of the Secure schema it conforms to. When the report is parsed, the file is validated using the appropriate schema and will be rejected if it does not succeed.\n"
  description="$description \n"
  description="$description A new version of the schema has been released, therefore in order to support analyzers using this version the new schema files must be included in the Rails codebase.\n"
  description="$description \n"
  description="$description ## Reference\n\n"
  description="$description The git branch and associated MR was automatically created by $CI_JOB_URL.\n"

  log "Creating MR to merge $BRANCH_NAME into master"
  local payload="{\"title\":\"$title\",\"source_branch\":\"$source_branch\",\"target_branch\":\"$target_branch\",\"assignee_id\":\"$assignee_id\",\"remove_source_branch\":\"$remove_source_branch\",\"description\":\"$description\",\"labels\":[\"devops::secure\",\"section::sec\",\"feature\",\"feature::maintenance\"]}"
  curl --fail --silent --show-error --request POST --header 'Content-Type:application/json' --data "$payload" --header "PRIVATE-TOKEN:$PUSH_TO_GITLAB_TOKEN" "$new_mr_url" | tee result.json

  log "MR created"
  echo "See $(jq -r '.web_url' <result.json)."
}

publish_schemas_to_gitlab_rails
create_mr_to_merge_schemas
